summary: Assert a user process can't write to a shared memory without proper privileges.
description: |
    Assert a user process can't write to a shared memory without proper privileges.
    The test first creates a PAGE_SIZE shared memory segment. Then it launches another
    process as a non-root user that tries to attach and write to that segment. Either of
    these operations is expected to fail and this process should be terminated properly.

    Test input:
        Four programs:
        - shm-create (using SysV shm) and shm-create-posix (using POSIX
        shm) that will create the shared memory segment under test.
        - shm-access (SysV) and shm-access-posix (POSIX) that will run with non-root
        privileges and assert either of the open/read/write operations fails.
        Input details:
        - adduser testuser
        - shm-create.c, source.
        - /tmp/shm-create create
        - su testuser -c /tmp/shm-access
        - /tmp/shm-create read
        - /tmp/shm-create-posix create
        - su testuser -c /tmp/shm-access-posix
        - /tmp/shm-create-posix read
    Expected results:
        If the non-root process can't access the shared memory segment as expected,
        you should expect the following output:
        [   PASS   ] :: Command 'gcc -o /tmp/shm-access -D_GNU_SOURCE shm-access.c' (Expected 0, got 0)
        shmat: Permission denied
        Attempt to open and attach shm segment...
        [   PASS   ] :: Command 'su testuser -c /tmp/shm-access' (Expected 1,139, got 1)
        [   PASS   ] :: Command '/tmp/shm-create read' (Expected 0, got 0)
        [   PASS   ] :: Command 'gcc -o /tmp/shm-access-posix -DUSE_POSIX_INTERFACE -D_GNU_SOURCE shm-access.c' (Expected 0, got 0)
        shm_open: Permission denied
        Attempt to open and attach shm segment...
        [   PASS   ] :: Command 'su testuser -c /tmp/shm-access-posix' (Expected 1,139, got 1)
        [   PASS   ] :: Command '/tmp/shm-create-posix read' (Expected 0, got 0)
    Results location:
        output.txt
contact: Pablo Ridolfi <pridolfi@redhat.com>
test: |
    if [ -n "${FFI_QM_SCENARIO}" ]; then
        podman exec --env TEST_DIR="$(pwd)" --env BEAKERLIB_DIR="$BEAKERLIB_DIR" \
          -t qm sh -c 'cd "$TEST_DIR"; bash runtest.sh'
    else
        bash ./runtest.sh
    fi
framework: beakerlib
adjust:
  require+:
    - beakerlib
    - gcc
    - glibc-devel
    - make
extra-summary: memory/mmra/shm-access
extra-task: memory/mmra/shm-access
enabled: true
duration: 10m
id: 2193c17f-ed9b-4a09-84f1-0e114011de8d
